local colors = require("colorscheme")
local wezterm = require("wezterm")
local mux = wezterm.mux

local dimmer = {
  brightness = 0.02,
  saturation = 1.0,
  hue = 1.0,
}

wezterm.on("gui-startup", function(cmd)
  --local tab, pane, window = mux.spawn_window(cmd or {})
  local _, _, window = mux.spawn_window(cmd or {})
  window:gui_window():toggle_fullscreen()
end)

return {
  colors = colors,
  --color_scheme = 'Ayu Mirage',
  --color_scheme = 'Gruvbox Dark',
  --color_scheme = 'tokyonight_dark',
  font = wezterm.font("Hack Nerd Font Propo", {
    weight = "Regular",
    style = "Italic",
  }),
  font_size = 14.0,
  line_height = 0.9,
  window_padding = {
    left = 5,
    right = 0,
    top = 0,
    bottom = 0,
  },
  -- Tab Bar
  --enable_tab_bar = false,
  hide_tab_bar_if_only_one_tab = true,
  show_tab_index_in_tab_bar = false,
  tab_bar_at_bottom = true,
  -- X11
  enable_wayland = true,
  -- Cursor style
  default_cursor_style = "SteadyBlock",
  --window_background_opacity = 0.9,
  window_background_image_hsb = dimmer,
  window_background_image = "/Users/merlo/Documents/dotfiles/wezterm/background2.jpeg",
}
