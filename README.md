# My Personal DotFiles
Before this plugins are configurated for my daily work, for nodejs, php, vuejs, typescript, markdown, lua, python and dart, you can customize using a folk or maybe creating a mergue request.

## NEOVIM
### Install Package Manager and language-servers
Make sure is installed in you computer [Yarn](https://yarnpkg.com/) , [Nodejs](https://nodejs.org/en/) prefered v16 and [python](https://python.org)

Only you need to copy my configurations then it installs automatically all necessary plugins to work.

### Additionally for some plugins depends for some plugins:

-For Telescope to make search by grep and images preview

```bash
apt update
apt install libx11-dev
apt install libxext-dev
sudo apt install libxres-dev
#to make work media-files telescope to show pictures in terminal
pip3 install ueberzug
```
-For Mac
```bash
brew install findutils
brew install libx11
brew install libxext
brew install tidy-html5
brew install jq
brew install curl
brew install wget
brew install fd
brew install ripgrep
brew install gettext
brew install fzf
brew install tmux
brew install coreutils
brew install composer # for php  projects
brew install cmake # for build nvim from github repository
brew install fnm # for nodejs

```


-For Telescope search using grep library
```bash
apt-get install ripgrep
apt install fd
```

-For Rest Client into Neovim

```bash
apt install tidy jq curl
```
-For Tmux, and make sure run this comand on start you terminal(generally in preferences you can set it, or with  alacritty already is setup)

```bash
apt install tmux
```

-For Powerline Font(necessary to show icons)

```bash
apt install fonts-powerline
```

-For FZF
```bash
apt install fzf
```

-Now open NEOVIM and run ":PackerInstall" and wait finish this and enjoy


NOTES: if you cant to change theme, run ":colorschemes gotham" and ":AirlineTheme gotham" to test on this way many combinatios that you want, on you tailored scheme replace it in .config/nvim/lua/plugins/theme.lua in the lines 17 and 18


## ALACRITTY

-For alacritty dependencies maybe if not you can't compile with cargo
```bash
apt install cmake freetype-devel fontconfig-devel libxcb-devel
apt-get install cmake pkg-config libfreetype6-dev libfontconfig1-dev libxcb-xfixes0-dev libxkbcommon-dev python3
cargo install alacritty
```
Set a shortcut on you linux to open alacritty.

## FISH
Make sure installed fish on you computer:
```bash
apt install fish
curl -sL https://git.io/fisher | source && fisher install jorgebucaran/fisher
fisher install jethrokuan/z
fisher install simnalamburt/shellder
```

## TO INSTALL WITH LINKS
```bash
ln -s $(pwd)/wezterm ~/.config/
ln -s $(pwd)/nvim ~/.config/
ln -s $(pwd)/ranger ~/.config/
ln -s $(pwd)/alacritty ~/.config/
ln -s $(pwd)/vifm ~/.config/
ln -s $(pwd)/btop ~/.config/
ln -s $(pwd)/tmux/.tmux.conf ~/
ln -s $(pwd)/tmux/.tmux ~/
ln -s $(pwd)/kitty ~/.config/
```

Thank's to see my configs.
## ABOUT ME
[LinkedIn: Rodolfo Merlo Ali](https://www.linkedin.com/in/rodolfo-merlo-ali/)
